# My SLide Decks

based on [Benji's Slide Decks](https://gitlab.com/benjifisher/slide-decks)

## [Übersicht](https://axel-klinger.gitlab.io/oer-slides/index.html)

- [Einen Kurs mit GitLab erstellen](https://axel-klinger.gitlab.io/oer-slides/oer-ws-1-haj.html) (2. Oktober 2019)
- [Einen Kurs mit GitLab erstellen](https://axel-klinger.gitlab.io/oer-slides/oer-ws-1.html) (13./14. Juni 2019)
- [Einen Jahrgang/Studiengang mit GitLab abbilden](https://axel-klinger.gitlab.io/oer-slides/oer-ws-2.html) (13./14. Juni 2019)
- [Ein Beispiel](https://axel-klinger.gitlab.io/oer-slides/oer-example.html) (13./14. Juni 2019)
- [Ein VSC Beispiel](https://axel-klinger.gitlab.io/oer-slides/test.html) (13./14. Juni 2019)
- [Kurse mit GitLab](https://axel-klinger.gitlab.io/oer-slides/oer-sc-19.html) (27. August 2019)
- [OER Werkzeuge](https://axel-klinger.gitlab.io/oer-slides/oer-werkzeuge.html) (30. Oktober 2019)
- [Vision für Open Science Projekte](https://axel-klinger.gitlab.io/oer-slides/opt-open-science.html) (23. November 2021)
