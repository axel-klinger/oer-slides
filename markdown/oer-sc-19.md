---
author: Axel Klinger
title: Kurse mit GitLab - OER Sommercamp 2019
date: 27. August 2019
revealjs-url: 'reveal.js'
theme: isovera
css:
  - 'https://fonts.googleapis.com/css?family=Roboto+Slab:700'
---

## Inhalt

- Warum GitLab
- Einordnung in Infrastrukur
- Vergleich mit Wiki & Co.

# Warum GitLab

## Vorteile von GitLab

- kostenloses Werkzeug im Netz
- Betrieb auf Schul/Bilsungs-Server möglich
- Zusammenarbeit im Netz
- Benutzerverwaltung
- Versionierung
- Trennung von Inhalt und Format
- geeignet für mobile Endgeräte
- freier Zugriff auf Inhalte

## Kurs aufbauen

- Einfacher Kurs
- Untermodule

## Markdown

* Überschriften
* Listen
* Bilder
* Videos
* u.v.a.m.
* https://docs.gitlab.com/ee/user/markdown.html

## Mitarbeit

- GitLab Account vorausgesetzt
- Issues erstellen
- Mergerequest
- direkte Mitarbeit am Projekt

# Ausblick

## OER-Plattform

* globale Auffindbarkeit
* Einstieg über landesweite Plattformen
* alle Arten von OER möglich

## Fragen?

* axel.klinger@tib.eu
