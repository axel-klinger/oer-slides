---
author: Axel Klinger
title: GitLab Workshop I - OERcamp Lübeck 2019
date: 13./14. Juni 2019
revealjs-url: 'reveal.js'
theme: isovera
css:
  - 'https://fonts.googleapis.com/css?family=Roboto+Slab:700'
---

## Inhalt

- Warum GitLab
- Anmeldung und erstes Projekt
- Projekt füllen
- Änderungsvorschläge einreichen
- Änderungen annehmen
- Mitarbeit erlauben
- Aufgaben (Issues)
- Versionen über Tags und Branches

# Warum GitLab

## Vorteile von GitLab

- kostenloses Werkzeug im Netz
- Betrieb auf Schul/Bilsungs-Server möglich
- Zusammenarbeit im Netz
- Benutzerverwaltung
- Versionierung
- Trennung von Inhalt und Format
- geeignet für mobile Endgeräte
- freier Zugriff auf Inhalte

## GitLab

- Registrieren
- Overview
- Projekt anlegen

## Kurs aufbauen

- Einfacher Kurs
- Untermodule

## Markdown

* Überschriften
* Listen
* Bilder
* Videos
* u.v.a.m.
* https://docs.gitlab.com/ee/user/markdown.html

## Änderungsvorschläge (MergeRequests)

- Fork erstellen
- Anpassungen machen
- Pull request erstellen

## Mitarbeit

- Berechtigung erteilen

# Weitere Themen

## Offline arbeiten

- Installation von Atom
- Bedienung des Editors
- Klonen eines Repositories
- Bearbeiten und einchecken

## Vorteile

* flüssigeres Arbeiten
* schnellere Vorschau

## Nachteile

* Risiko von Mergekonflikten
* erfordert mehr Einarbeitung

# Ausblick

## OER-Plattform

* globale Auffindbarkeit
* Einstieg über landesweite Plattformen
* alle Arten von OER möglich

## Fragen?

* axel.klinger@tib.eu
